# Usage

1. clone [`qmk_firmware`](https://github.com/qmk/qmk_firmware) inside this repo
1. make symlink
   ```shell
   ln -s $PWD/keyboard qmk_firmware/keyboards/keyboard
   ```

Compile the keyboard with

```shell
cd qmk_firmware
qmk compile -kb keyboard/ergonomic/3x6/blackpill -km default
```

> The keyboard files are in `keyboard`

To flash on Geehy APM32 board, use the following from the root folder

```shell
openocd -f flash_apm32.cfg
```

## Black magic probe

SWD Pins (from `src/platforms/native/platform.h` in blackmagic repository)

```
TMS      = PA4  (input/output for SWDIO)
TCK      = PA5  (output SWCLK)
```

# More resources

## OpenOCD

- https://openocd.org/doc-release/html/Flash-Commands.html

## Black magic probe

- Black magic probe dfu, required for working https://gist.github.com/micooke/e9a4cf831316ace10ee6506ebbab95ed
- http://nuft.github.io/arm/2015/08/24/blackmagic-stlink.html
- https://microcontrollerelectronics.com/how-to-convert-an-stm32f103c8t6-into-a-black-magic-probe/
- https://primalcortex.wordpress.com/2017/06/13/building-a-black-magic-debug-probe/
