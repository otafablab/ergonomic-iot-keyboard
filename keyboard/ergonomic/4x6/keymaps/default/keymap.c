/**
 * Copyright 2022 Charly Delay <charly@codesink.dev> (@0xcharly)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include QMK_KEYBOARD_H

enum charybdis_keymap_layers {
  LAYER_BASE = 0,
  /* LAYER_LOWER, */
  /* LAYER_RAISE, */
};

/* #define LOWER MO(LAYER_LOWER) */
/* #define RAISE MO(LAYER_RAISE) */
/* #define LOWER MO(LAYER_BASE) */
/* #define RAISE MO(LAYER_BASE) */

// clang-format off
const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
  [LAYER_BASE] = {
  { XXXXXXX      , XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, KC_BSPC, /**/ },
  { XXXXXXX      , KC_Q,    KC_W,    KC_E,    KC_R,    KC_T,    KC_SPC,  /**/ },
  { KC_BSPC      , KC_A,    KC_S,    KC_D,    KC_F,    KC_G, XXXXXXX,    /**/ },
  { XXXXXXX      , KC_Z,    KC_X,    KC_C,    KC_V,    KC_B, XXXXXXX,    /**/ },
  { KC_NO        , KC_NO,   KC_NO,   KC_NO,   KC_NO,  KC_NO, XXXXXXX,   /**/},
  { KC_NO, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX },
  { KC_ENT     ,      KC_Y,    KC_U,    KC_I,    KC_O,    KC_P, XXXXXXX,   },
  { KC_RIGHT_ALT,     KC_H       ,    KC_J,    KC_K,    KC_L, KC_SCLN, XXXXXXX,                },
  { XXXXXXX   ,       KC_N ,    KC_M, KC_COMM,  KC_DOT, KC_SLSH, XXXXXXX,                },
  {   KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO },
  }

/*   [LAYER_LOWER] = LAYOUT( */
/*   // ╭─────────────────────────────────────────────╮ ╭─────────────────────────────────────────────╮ */
/*        XXXXXXX, KC_MNXT, KC_MPLY, KC_MPRV, XXXXXXX,    KC_LBRC,    KC_7,    KC_8,    KC_9, XXXXXXX, */
/*   // ├─────────────────────────────────────────────┤ ├─────────────────────────────────────────────┤ */
/*        XXXXXXX, KC_LALT, KC_LCTL, KC_LSFT, XXXXXXX,    KC_PPLS,    KC_4,    KC_5,    KC_6, XXXXXXX, */
/*   // ├─────────────────────────────────────────────┤ ├─────────────────────────────────────────────┤ */
/*        XXXXXXX, XXXXXXX, XXXXXXX, EE_CLR,  QK_BOOT,    KC_PAST,    KC_1,    KC_2,    KC_3, XXXXXXX, */
/*   // ╰─────────────────────────────────────────────┤ ├─────────────────────────────────────────────╯ */
/*                          XXXXXXX, XXXXXXX, _______,    _______, XXXXXXX, XXXXXXX */
/* //                     ╰───────────────────────────╯ ╰───────────────────────────╯ */
/*   ), */

/*   [LAYER_RAISE] = LAYOUT( */
/*   // ╭─────────────────────────────────────────────╮ ╭─────────────────────────────────────────────╮ */
/*        XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,    XXXXXXX, KC_VOLU, KC_MUTE, KC_VOLD, XXXXXXX, */
/*   // ├─────────────────────────────────────────────┤ ├─────────────────────────────────────────────┤ */
/*        XXXXXXX,   KC_UP, KC_DOWN, KC_RGHT, XXXXXXX,    XXXXXXX, KC_RSFT, KC_RCTL, KC_RALT, XXXXXXX, */
/*   // ├─────────────────────────────────────────────┤ ├─────────────────────────────────────────────┤ */
/*        XXXXXXX, KC_PGUP, KC_PGDN,  KC_END, XXXXXXX,    QK_BOOT, EE_CLR,  XXXXXXX, XXXXXXX, XXXXXXX, */
/*   // ╰─────────────────────────────────────────────┤ ├─────────────────────────────────────────────╯ */
/*                          XXXXXXX, XXXXXXX, _______,    _______, XXXXXXX, XXXXXXX */
/* //                     ╰───────────────────────────╯ ╰───────────────────────────╯ */
/*   ), */
};
// clang-format on
